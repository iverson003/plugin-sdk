'use strict';
!function(request, __webpack_require__) {
  module.exports = __webpack_require__(request);
}(window, function(req, canCreateDiscussions) {
  /**
   * @param {string} name
   * @param {undefined} data
   * @param {!Object} value
   * @return {undefined}
   */
  function callback(name, data, value) {
    if (req.WeixinJSBridge) {
      WeixinJSBridge.invoke(name, send(data), function(error) {
        test(name, error, value);
      });
    } else {
      add(name, value);
    }
  }
  /**
   * @param {string} link
   * @param {!Object} type
   * @param {!Object} options
   * @return {undefined}
   */
  function init(link, type, options) {
    if (req.WeixinJSBridge) {
      WeixinJSBridge.on(link, function(e) {
        if (options && options.trigger) {
          options.trigger(e);
        }
        test(link, e, type);
      });
    } else {
      add(link, options || type);
    }
  }
  /**
   * @param {number} data
   * @return {?}
   */
  function send(data) {
    return (data = data || {}).appId = options.appId, data.verifyAppId = options.appId, data.verifySignType = "sha1", data.verifyTimestamp = options.timestamp + "", data.verifyNonceStr = options.nonceStr, data.verifySignature = options.signature, data;
  }
  /**
   * @param {!Object} data
   * @return {?}
   */
  function save(data) {
    return {
      timeStamp : data.timestamp + "",
      nonceStr : data.nonceStr,
      package : data.package,
      paySign : data.paySign,
      signType : data.signType || "SHA1"
    };
  }
  /**
   * @param {string} e
   * @param {!Object} data
   * @param {!Object} status
   * @return {undefined}
   */
  function test(e, data, status) {
    if (!("openEnterpriseChat" != e && "openBusinessView" !== e)) {
      data.errCode = data.err_code;
    }
    delete data.err_code;
    delete data.err_desc;
    delete data.err_detail;
    var msg = data.errMsg;
    if (!msg) {
      msg = data.err_msg;
      delete data.err_msg;
      msg = function(fn, html) {
        /** @type {string} */
        var name = fn;
        var line = res[name];
        if (line) {
          name = line;
        }
        /** @type {string} */
        var result = "ok";
        if (html) {
          var end = html.indexOf(":");
          if ("confirm" == (result = html.substring(end + 1))) {
            /** @type {string} */
            result = "ok";
          }
          if ("failed" == result) {
            /** @type {string} */
            result = "fail";
          }
          if (-1 != result.indexOf("failed_")) {
            result = result.substring(7);
          }
          if (-1 != result.indexOf("fail_")) {
            result = result.substring(5);
          }
          if (!("access denied" != (result = (result = result.replace(/_/g, " ")).toLowerCase()) && "no permission to execute" != result)) {
            /** @type {string} */
            result = "permission denied";
          }
          if ("config" == name && "function not exist" == result) {
            /** @type {string} */
            result = "ok";
          }
          if ("" == result) {
            /** @type {string} */
            result = "fail";
          }
        }
        return html = name + ":" + result;
      }(e, msg);
      data.errMsg = msg;
    }
    if ((status = status || {})._complete) {
      status._complete(data);
      delete status._complete;
    }
    msg = data.errMsg || "";
    if (options.debug && !status.isInnerInvoke) {
      alert(JSON.stringify(data));
    }
    var partStart = msg.indexOf(":");
    switch(msg.substring(partStart + 1)) {
      case "ok":
        if (status.success) {
          status.success(data);
        }
        break;
      case "cancel":
        if (status.cancel) {
          status.cancel(data);
        }
        break;
      default:
        if (status.fail) {
          status.fail(data);
        }
    }
    if (status.complete) {
      status.complete(data);
    }
  }
  /**
   * @param {!NodeList} value
   * @return {?}
   */
  function done(value) {
    if (value) {
      /** @type {number} */
      var i = 0;
      var valueLength = value.length;
      for (; i < valueLength; ++i) {
        var prop = value[i];
        var length = wx[prop];
        if (length) {
          value[i] = length;
        }
      }
      return value;
    }
  }
  /**
   * @param {string} value
   * @param {string} type
   * @return {undefined}
   */
  function add(value, type) {
    if (!(!options.debug || type && type.isInnerInvoke)) {
      var field = res[value];
      if (field) {
        value = field;
      }
      if (type && type._complete) {
        delete type._complete;
      }
      console.log('"' + value + '",', type || "");
    }
  }
  /**
   * @return {?}
   */
  function extend() {
    return (new Date).getTime();
  }
  /**
   * @param {!Function} callback
   * @return {undefined}
   */
  function start(callback) {
    if (target) {
      if (req.WeixinJSBridge) {
        callback();
      } else {
        if (doc.addEventListener) {
          doc.addEventListener("WeixinJSBridgeReady", callback, false);
        }
      }
    }
  }
  if (!req.jWeixin) {
    var match;
    var wx = {
      config : "preVerifyJSAPI",
      onMenuShareTimeline : "menu:share:timeline",
      onMenuShareAppMessage : "menu:share:appmessage",
      onMenuShareQQ : "menu:share:qq",
      onMenuShareWeibo : "menu:share:weiboApp",
      onMenuShareQZone : "menu:share:QZone",
      previewImage : "imagePreview",
      getLocation : "geoLocation",
      openProductSpecificView : "openProductViewWithPid",
      addCard : "batchAddCard",
      openCard : "batchViewCard",
      chooseWXPay : "getBrandWCPayRequest",
      openEnterpriseRedPacket : "getRecevieBizHongBaoRequest",
      startSearchBeacons : "startMonitoringBeacons",
      stopSearchBeacons : "stopMonitoringBeacons",
      onSearchBeacons : "onBeaconsInRange",
      consumeAndShareCard : "consumedShareCard",
      openAddress : "editAddress"
    };
    var res = function() {
      var new_obj = {};
      var prop;
      for (prop in wx) {
        /** @type {string} */
        new_obj[wx[prop]] = prop;
      }
      return new_obj;
    }();
    var doc = req.document;
    var title = doc.title;
    /** @type {string} */
    var ua = navigator.userAgent.toLowerCase();
    /** @type {string} */
    var exports = navigator.platform.toLowerCase();
    /** @type {boolean} */
    var breadcrumbRootName = !(!exports.match("mac") && !exports.match("win"));
    /** @type {boolean} */
    var host = -1 != ua.indexOf("wxdebugger");
    /** @type {boolean} */
    var target = -1 != ua.indexOf("micromessenger");
    /** @type {boolean} */
    var rawDataIsList = -1 != ua.indexOf("android");
    /** @type {boolean} */
    var rawDataIsArray = -1 != ua.indexOf("iphone") || -1 != ua.indexOf("ipad");
    /** @type {string} */
    var version = (match = ua.match(/micromessenger\/(\d+\.\d+\.\d+)/) || ua.match(/micromessenger\/(\d+\.\d+)/)) ? match[1] : "";
    var cl = {
      initStartTime : extend(),
      initEndTime : 0,
      preVerifyStartTime : 0,
      preVerifyEndTime : 0
    };
    var data = {
      version : 1,
      appId : "",
      initTime : 0,
      preVerifyTime : 0,
      networkType : "",
      isPreVerifyOk : 1,
      systemType : rawDataIsArray ? 1 : rawDataIsList ? 2 : -1,
      clientVersion : version,
      url : encodeURIComponent(location.href)
    };
    var options = {};
    var self = {
      _completes : []
    };
    var node = {
      state : 0,
      data : {}
    };
    start(function() {
      cl.initEndTime = extend();
    });
    /** @type {boolean} */
    var I = false;
    /** @type {!Array} */
    var opts = [];
    var service = {
      config : function(port) {
        add("config", options = port);
        /** @type {boolean} */
        var t = false !== options.check;
        start(function() {
          if (t) {
            callback(wx.config, {
              verifyJsApiList : done(options.jsApiList),
              verifyOpenTagList : done(options.openTagList)
            }, function() {
              /**
               * @param {!Object} data
               * @return {undefined}
               */
              self._complete = function(data) {
                cl.preVerifyEndTime = extend();
                /** @type {number} */
                node.state = 1;
                /** @type {!Object} */
                node.data = data;
              };
              /**
               * @param {!Object} bufDesc
               * @return {undefined}
               */
              self.success = function(bufDesc) {
                /** @type {number} */
                data.isPreVerifyOk = 0;
              };
              /**
               * @param {!Object} message
               * @return {undefined}
               */
              self.fail = function(message) {
                if (self._fail) {
                  self._fail(message);
                } else {
                  /** @type {number} */
                  node.state = -1;
                }
              };
              /** @type {!Array} */
              var t = self._completes;
              return t.push(function() {
                !function() {
                  if (!(breadcrumbRootName || host || options.debug || version < "6.0.2" || data.systemType < 0)) {
                    /** @type {!Image} */
                    var img = new Image;
                    data.appId = options.appId;
                    /** @type {number} */
                    data.initTime = cl.initEndTime - cl.initStartTime;
                    /** @type {number} */
                    data.preVerifyTime = cl.preVerifyEndTime - cl.preVerifyStartTime;
                    service.getNetworkType({
                      isInnerInvoke : true,
                      success : function(e) {
                        data.networkType = e.networkType;
                        /** @type {string} */
                        var normalSrc = "https://open.weixin.qq.com/sdk/report?v=" + data.version + "&o=" + data.isPreVerifyOk + "&s=" + data.systemType + "&c=" + data.clientVersion + "&a=" + data.appId + "&n=" + data.networkType + "&i=" + data.initTime + "&p=" + data.preVerifyTime + "&u=" + data.url;
                        /** @type {string} */
                        img.src = normalSrc;
                      }
                    });
                  }
                }();
              }), self.complete = function(overwrite) {
                /** @type {number} */
                var j = 0;
                /** @type {number} */
                var i = t.length;
                for (; j < i; ++j) {
                  t[j]();
                }
                /** @type {!Array} */
                self._completes = [];
              }, self;
            }());
            cl.preVerifyStartTime = extend();
          } else {
            /** @type {number} */
            node.state = 1;
            /** @type {!Array} */
            var i = self._completes;
            /** @type {number} */
            var g = 0;
            /** @type {number} */
            var e = i.length;
            for (; g < e; ++g) {
              i[g]();
            }
            /** @type {!Array} */
            self._completes = [];
          }
        });
        if (!service.invoke) {
          /**
           * @param {string} fn
           * @param {undefined} data
           * @param {!Function} callback
           * @return {undefined}
           */
          service.invoke = function(fn, data, callback) {
            if (req.WeixinJSBridge) {
              WeixinJSBridge.invoke(fn, send(data), callback);
            }
          };
          /**
           * @param {string} type
           * @param {!Function} data
           * @return {undefined}
           */
          service.on = function(type, data) {
            if (req.WeixinJSBridge) {
              WeixinJSBridge.on(type, data);
            }
          };
        }
      },
      ready : function(fn) {
        if (0 != node.state) {
          fn();
        } else {
          self._completes.push(fn);
          if (!target && options.debug) {
            fn();
          }
        }
      },
      error : function(func) {
        if (!(version < "6.0.2")) {
          if (-1 == node.state) {
            func(node.data);
          } else {
            /** @type {!Function} */
            self._fail = func;
          }
        }
      },
      checkJsApi : function(status) {
        callback("checkJsApi", {
          jsApiList : done(status.jsApiList)
        }, (status._complete = function(args) {
          if (rawDataIsList) {
            var changes = args.checkResult;
            if (changes) {
              /** @type {*} */
              args.checkResult = JSON.parse(changes);
            }
          }
          args = function(args) {
            var times = args.checkResult;
            var i;
            for (i in times) {
              var t = res[i];
              if (t) {
                times[t] = times[i];
                delete times[i];
              }
            }
            return args;
          }(args);
        }, status));
      },
      onMenuShareTimeline : function(options) {
        init(wx.onMenuShareTimeline, {
          complete : function() {
            callback("shareTimeline", {
              title : options.title || title,
              desc : options.title || title,
              img_url : options.imgUrl || "",
              link : options.link || location.href,
              type : options.type || "link",
              data_url : options.dataUrl || ""
            }, options);
          }
        }, options);
      },
      onMenuShareAppMessage : function(options) {
        init(wx.onMenuShareAppMessage, {
          complete : function(req) {
            if ("favorite" === req.scene) {
              callback("sendAppMessage", {
                title : options.title || title,
                desc : options.desc || "",
                link : options.link || location.href,
                img_url : options.imgUrl || "",
                type : options.type || "link",
                data_url : options.dataUrl || ""
              });
            } else {
              callback("sendAppMessage", {
                title : options.title || title,
                desc : options.desc || "",
                link : options.link || location.href,
                img_url : options.imgUrl || "",
                type : options.type || "link",
                data_url : options.dataUrl || ""
              }, options);
            }
          }
        }, options);
      },
      onMenuShareQQ : function(options) {
        init(wx.onMenuShareQQ, {
          complete : function() {
            callback("shareQQ", {
              title : options.title || title,
              desc : options.desc || "",
              img_url : options.imgUrl || "",
              link : options.link || location.href
            }, options);
          }
        }, options);
      },
      onMenuShareWeibo : function(options) {
        init(wx.onMenuShareWeibo, {
          complete : function() {
            callback("shareWeiboApp", {
              title : options.title || title,
              desc : options.desc || "",
              img_url : options.imgUrl || "",
              link : options.link || location.href
            }, options);
          }
        }, options);
      },
      onMenuShareQZone : function(options) {
        init(wx.onMenuShareQZone, {
          complete : function() {
            callback("shareQZone", {
              title : options.title || title,
              desc : options.desc || "",
              img_url : options.imgUrl || "",
              link : options.link || location.href
            }, options);
          }
        }, options);
      },
      updateTimelineShareData : function(data) {
        callback("updateTimelineShareData", {
          title : data.title,
          link : data.link,
          imgUrl : data.imgUrl
        }, data);
      },
      updateAppMessageShareData : function(data) {
        callback("updateAppMessageShareData", {
          title : data.title,
          desc : data.desc,
          link : data.link,
          imgUrl : data.imgUrl
        }, data);
      },
      startRecord : function(data) {
        callback("startRecord", {}, data);
      },
      stopRecord : function(source) {
        callback("stopRecord", {}, source);
      },
      onVoiceRecordEnd : function(block) {
        init("onVoiceRecordEnd", block);
      },
      playVoice : function(data) {
        callback("playVoice", {
          localId : data.localId
        }, data);
      },
      pauseVoice : function(val) {
        callback("pauseVoice", {
          localId : val.localId
        }, val);
      },
      stopVoice : function(val) {
        callback("stopVoice", {
          localId : val.localId
        }, val);
      },
      onVoicePlayEnd : function(block) {
        init("onVoicePlayEnd", block);
      },
      uploadVoice : function(val) {
        callback("uploadVoice", {
          localId : val.localId,
          isShowProgressTips : 0 == val.isShowProgressTips ? 0 : 1
        }, val);
      },
      downloadVoice : function(server) {
        callback("downloadVoice", {
          serverId : server.serverId,
          isShowProgressTips : 0 == server.isShowProgressTips ? 0 : 1
        }, server);
      },
      translateVoice : function(val) {
        callback("translateVoice", {
          localId : val.localId,
          isShowProgressTips : 0 == val.isShowProgressTips ? 0 : 1
        }, val);
      },
      chooseImage : function(param) {
        callback("chooseImage", {
          scene : "1|2",
          count : param.count || 9,
          sizeType : param.sizeType || ["original", "compressed"],
          sourceType : param.sourceType || ["album", "camera"]
        }, (param._complete = function(uri) {
          if (rawDataIsList) {
            var path = uri.localIds;
            try {
              if (path) {
                /** @type {*} */
                uri.localIds = JSON.parse(path);
              }
            } catch (e) {
            }
          }
        }, param));
      },
      getLocation : function(data) {
      },
      previewImage : function(data) {
        callback(wx.previewImage, {
          current : data.current,
          urls : data.urls
        }, data);
      },
      uploadImage : function(data) {
        callback("uploadImage", {
          localId : data.localId,
          isShowProgressTips : 0 == data.isShowProgressTips ? 0 : 1
        }, data);
      },
      downloadImage : function(e) {
        callback("downloadImage", {
          serverId : e.serverId,
          isShowProgressTips : 0 == e.isShowProgressTips ? 0 : 1
        }, e);
      },
      getLocalImgData : function(val) {
        if (false === I) {
          /** @type {boolean} */
          I = true;
          callback("getLocalImgData", {
            localId : val.localId
          }, (val._complete = function(data) {
            if (I = false, 0 < opts.length) {
              var error = opts.shift();
              wx.getLocalImgData(error);
            }
          }, val));
        } else {
          opts.push(val);
        }
      },
      getNetworkType : function(data) {
        callback("getNetworkType", {}, (data._complete = function(success) {
          success = function(result) {
            var line = result.errMsg;
            /** @type {string} */
            result.errMsg = "getNetworkType:ok";
            var type = result.subtype;
            if (delete result.subtype, type) {
              result.networkType = type;
            } else {
              var secondSpacePos = line.indexOf(":");
              var type = line.substring(secondSpacePos + 1);
              switch(type) {
                case "wifi":
                case "edge":
                case "wwan":
                  result.networkType = type;
                  break;
                default:
                  /** @type {string} */
                  result.errMsg = "getNetworkType:fail";
              }
            }
            return result;
          }(success);
        }, data));
      },
      openLocation : function(data) {
        callback("openLocation", {
          latitude : data.latitude,
          longitude : data.longitude,
          name : data.name || "",
          address : data.address || "",
          scale : data.scale || 28,
          infoUrl : data.infoUrl || ""
        }, data);
      },
      getLocation : function(token) {
        callback(wx.getLocation, {
          type : (token = token || {}).type || "wgs84"
        }, (token._complete = function(data) {
          delete data.type;
        }, token));
      },
      hideOptionMenu : function(productInfo) {
        callback("hideOptionMenu", {}, productInfo);
      },
      showOptionMenu : function(productInfo) {
        callback("showOptionMenu", {}, productInfo);
      },
      closeWindow : function(state) {
        callback("closeWindow", {}, state = state || {});
      },
      hideMenuItems : function(r) {
        callback("hideMenuItems", {
          menuList : r.menuList
        }, r);
      },
      showMenuItems : function(r) {
        callback("showMenuItems", {
          menuList : r.menuList
        }, r);
      },
      hideAllNonBaseMenuItem : function(productInfo) {
        callback("hideAllNonBaseMenuItem", {}, productInfo);
      },
      showAllNonBaseMenuItem : function(productInfo) {
        callback("showAllNonBaseMenuItem", {}, productInfo);
      },
      scanQRCode : function(status) {
        callback("scanQRCode", {
          needResult : (status = status || {}).needResult || 0,
          scanType : status.scanType || ["qrCode", "barCode"]
        }, (status._complete = function(data) {
          if (rawDataIsArray) {
            var body = data.resultStr;
            if (body) {
              /** @type {*} */
              var events = JSON.parse(body);
              /** @type {*} */
              data.resultStr = events && events.scan_code && events.scan_code.scan_result;
            }
          }
        }, status));
      },
      openAddress : function(status) {
        callback(wx.openAddress, {}, (status._complete = function(obj) {
          obj = function(address) {
            return address.postalCode = address.addressPostalCode, delete address.addressPostalCode, address.provinceName = address.proviceFirstStageName, delete address.proviceFirstStageName, address.cityName = address.addressCitySecondStageName, delete address.addressCitySecondStageName, address.countryName = address.addressCountiesThirdStageName, delete address.addressCountiesThirdStageName, address.detailInfo = address.addressDetailInfo, delete address.addressDetailInfo, address;
          }(obj);
        }, status));
      },
      openProductSpecificView : function(data) {
        callback(wx.openProductSpecificView, {
          pid : data.productId,
          view_type : data.viewType || 0,
          ext_info : data.extInfo
        }, data);
      },
      addCard : function(status) {
        var state = status.cardList;
        /** @type {!Array} */
        var transactionIDList = [];
        /** @type {number} */
        var j = 0;
        var count = state.length;
        for (; j < count; ++j) {
          var card = state[j];
          var data = {
            card_id : card.cardId,
            card_ext : card.cardExt
          };
          transactionIDList.push(data);
        }
        callback(wx.addCard, {
          card_list : transactionIDList
        }, (status._complete = function(options) {
          var data = options.card_list;
          if (data) {
            /** @type {number} */
            var i = 0;
            var patchLen = (data = JSON.parse(data)).length;
            for (; i < patchLen; ++i) {
              var card = data[i];
              card.cardId = card.card_id;
              card.cardExt = card.card_ext;
              /** @type {boolean} */
              card.isSuccess = !!card.is_succ;
              delete card.card_id;
              delete card.card_ext;
              delete card.is_succ;
            }
            /** @type {*} */
            options.cardList = data;
            delete options.card_list;
          }
        }, status));
      },
      chooseCard : function(data) {
        callback("chooseCard", {
          app_id : options.appId,
          location_id : data.shopId || "",
          sign_type : data.signType || "SHA1",
          card_id : data.cardId || "",
          card_type : data.cardType || "",
          card_sign : data.cardSign,
          time_stamp : data.timestamp + "",
          nonce_str : data.nonceStr
        }, (data._complete = function(line) {
          line.cardList = line.choose_card_info;
          delete line.choose_card_info;
        }, data));
      },
      openCard : function(type) {
        var properties = type.cardList;
        /** @type {!Array} */
        var transactionIDList = [];
        /** @type {number} */
        var j = 0;
        var pl = properties.length;
        for (; j < pl; ++j) {
          var event = properties[j];
          var data = {
            card_id : event.cardId,
            code : event.code
          };
          transactionIDList.push(data);
        }
        callback(wx.openCard, {
          card_list : transactionIDList
        }, type);
      },
      consumeAndShareCard : function(data) {
        callback(wx.consumeAndShareCard, {
          consumedCardId : data.cardId,
          consumedCode : data.code
        }, data);
      },
      chooseWXPay : function(state) {
        callback(wx.chooseWXPay, save(state), state);
      },
      openEnterpriseRedPacket : function(state) {
        callback(wx.openEnterpriseRedPacket, save(state), state);
      },
      startSearchBeacons : function(d) {
        callback(wx.startSearchBeacons, {
          ticket : d.ticket
        }, d);
      },
      stopSearchBeacons : function(productInfo) {
        callback(wx.stopSearchBeacons, {}, productInfo);
      },
      onSearchBeacons : function(block) {
        init(wx.onSearchBeacons, block);
      },
      openEnterpriseChat : function(info) {
        callback("openEnterpriseChat", {
          useridlist : info.userIds,
          chatname : info.groupName
        }, info);
      },
      launchMiniProgram : function(message) {
        callback("launchMiniProgram", {
          targetAppId : message.targetAppId,
          path : function(bytes) {
            if ("string" == typeof bytes && 0 < bytes.length) {
              /** @type {string} */
              var value = bytes.split("?")[0];
              /** @type {string} */
              var text = bytes.split("?")[1];
              return value = value + ".html", void 0 !== text ? value + "?" + text : value;
            }
          }(message.path),
          envVersion : message.envVersion
        }, message);
      },
      openBusinessView : function(self) {
        callback("openBusinessView", {
          businessType : self.businessType,
          queryString : self.queryString || "",
          envVersion : self.envVersion
        }, (self._complete = function(data) {
          if (rawDataIsList) {
            var dpinBox = data.extraData;
            if (dpinBox) {
              try {
                /** @type {*} */
                data.extraData = JSON.parse(dpinBox);
              } catch (e) {
                data.extraData = {};
              }
            }
          }
        }, self));
      },
      miniProgram : {
        navigateBack : function(data) {
          data = data || {};
          start(function() {
            callback("invokeMiniProgramAPI", {
              name : "navigateBack",
              arg : {
                delta : data.delta || 1
              }
            }, data);
          });
        },
        navigateTo : function(row) {
          start(function() {
            callback("invokeMiniProgramAPI", {
              name : "navigateTo",
              arg : {
                url : row.url
              }
            }, row);
          });
        },
        redirectTo : function(response) {
          start(function() {
            callback("invokeMiniProgramAPI", {
              name : "redirectTo",
              arg : {
                url : response.url
              }
            }, response);
          });
        },
        switchTab : function(data) {
          start(function() {
            callback("invokeMiniProgramAPI", {
              name : "switchTab",
              arg : {
                url : data.url
              }
            }, data);
          });
        },
        reLaunch : function(version) {
          start(function() {
            callback("invokeMiniProgramAPI", {
              name : "reLaunch",
              arg : {
                url : version.url
              }
            }, version);
          });
        },
        postMessage : function(success) {
          start(function() {
            callback("invokeMiniProgramAPI", {
              name : "postMessage",
              arg : success.data || {}
            }, success);
          });
        },
        getEnv : function(jsObjectToElmDict) {
          start(function() {
            jsObjectToElmDict({
              miniprogram : "miniprogram" === req.__wxjs_environment
            });
          });
        }
      }
    };
    /** @type {number} */
    var _i = 1;
    var siblingsToAdd = {};
    return doc.addEventListener("error", function(event) {
      if (!rawDataIsList) {
        var t = event.target;
        var id = t.tagName;
        var script = t.src;
        if ("IMG" == id || "VIDEO" == id || "AUDIO" == id || "SOURCE" == id) {
          if (-1 != script.indexOf("wxlocalresource://")) {
            event.preventDefault();
            event.stopPropagation();
            var i = t["wx-id"];
            if (i || (i = _i++, t["wx-id"] = i), siblingsToAdd[i]) {
              return;
            }
            /** @type {boolean} */
            siblingsToAdd[i] = true;
            wx.ready(function() {
              wx.getLocalImgData({
                localId : script,
                success : function(engine) {
                  t.src = engine.localData;
                }
              });
            });
          }
        }
      }
    }, true), doc.addEventListener("load", function(e) {
      if (!rawDataIsList) {
        var n = e.target;
        var nodeName = n.tagName;
        n.src;
        if ("IMG" == nodeName || "VIDEO" == nodeName || "AUDIO" == nodeName || "SOURCE" == nodeName) {
          var i = n["wx-id"];
          if (i) {
            /** @type {boolean} */
            siblingsToAdd[i] = false;
          }
        }
      }
    }, true), canCreateDiscussions && (req.wx = req.jWeixin = service), service;
  }
});