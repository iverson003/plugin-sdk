// 注入代码
const appType = navigator.userAgent
const WKBridgeEvent = {
  _listeners: {},
  addEvent(callBackID, fn) {
    this._listeners[callBackID] = fn
    return this
  },
  fireEvent(callBackID, param) {
    const fn = this._listeners[callBackID]
    if (typeof callBackID === "string" && typeof fn === "function") {
      fn(JSON.parse(param))
    } else {
      delete this._listeners[callBackID]
    }
    return this
  },
  removeEvent(callBackID) {
    delete this._listeners[callBackID]
    return this
  }
}

const zhiting = {
  invoke(funcName, params, callback) {
    let message = {}
    let timeStamp = new Date().getTime()
    const callbackID = `${funcName}_${timeStamp}_callback`
    if (callback) {
      if (!WKBridgeEvent._listeners[callbackID]) {
        WKBridgeEvent.addEvent(callbackID, function (data) {
          callback(data)
        })
      }
    }
    if (callback) {
      message = {
        func: funcName,
        params,
        callbackID
      }
    } else {
      message = {
        func: funcName,
        params: params
      }
    }
    // 判断ios和android
    const isIOS = !!appType.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)
    if (isIOS) {
      // ios
      window.webkit.messageHandlers.WKEventHandler.postMessage(message);
    } else {
      // android
      zhitingApp.entry(JSON.stringify(message));
    }
  },
  callBack(callBackID, data, noFire) {
    WKBridgeEvent.fireEvent(callBackID, data)
    if (!noFire) {
      WKBridgeEvent.removeEvent(callBackID)
    }
  },
  removeAllCallBacks(data) {
    WKBridgeEvent._listeners = {}
  }
}

// 判断是否在app内
const isApp = () => {
  const ua = navigator.userAgent
  if (ua.includes('zhitingua')) {
    return true
  }
  return false
}

// 调用app方法的总入口
const busEntry = (name, data, callback) => {
  if (isApp()) {
    window.zhiting.invoke(name, data, callback)
  } else {
    mock.invoke(name, data, callback)
    console.error('please make sure to call in the zhiting app:' + name)
  }
}

// mock数据
const mock = {
  invoke: (name, data, callback) => {
    if (typeof callback === 'function') {
      const len = Object.keys(data).length
      if (len) {
        callback(data)
      } else {
        callback({ status: 0, data: '测试成功' })
      }
    }
  }
}

// 原生方法聚合对象
const zt = {
  getNetworkType: (data, callback) => {
    busEntry('networkType', data, callback)
  },
  setTitle: (data, callback) => {
    busEntry('setTitle', data, callback)
  },
  getUserInfo: (data, callback) => {
    busEntry('getUserInfo', data, callback)
  },
  connectDeviceByBluetooth: (data, callback) => {
    busEntry('connectDeviceByBluetooth', data, callback)
  },
  connectNetworkByBluetooth: (data, callback) => {
    busEntry('connectNetworkByBluetooth', data, callback)
  },
  connectDeviceHotspot: (data, callback) => {
    busEntry('connectDeviceHotspot', data, callback)
  },
  createDeviceByHotspot: (data, callback) => {
    busEntry('createDeviceByHotspot', data, callback)
  },
  connectDeviceByHotspot: (data, callback) => {
    busEntry('connectDeviceByHotspot', data, callback)
  },
  connectNetworkByHotspot: (data, callback) => {
    busEntry('connectNetworkByHotspot', data, callback)
  },
  getDeviceInfo: (data, callback) => {
    busEntry('getDeviceInfo', data, callback)
  },
  getConnectWifi: (data, callback) => {
    busEntry('getConnectWifi', data, callback)
  },
  getSystemWifiList: (data, callback) => {
    busEntry('getSystemWifiList', data, callback)
  },
  toSystemWlan: (data, callback) => {
    busEntry('toSystemWlan', data, callback)
  },
  getSocketAddress: (data, callback) => {
    busEntry('getSocketAddress', data, callback)
  },
  connectSocket: (data, callback) => {
    busEntry('connectSocket', data, callback)
  },
  sendSocketMessage: (data, callback) => {
    busEntry('sendSocketMessage', data, callback)
  },
  onSocketOpen: (data, callback) => {
    busEntry('onSocketOpen', data, callback)
  },
  onSocketMessage: (data, callback) => {
    busEntry('onSocketMessage', data, callback)
  },
  onSocketError: (data, callback) => {
    busEntry('onSocketError', data, callback)
  },
  onSocketClose: (data, callback) => {
    busEntry('onSocketClose', data, callback)
  },
  closeSocket: (data, callback) => {
    busEntry('closeSocket', data, callback)
  },
  registerDeviceByHotspot: (data, callback) => {
    busEntry('registerDeviceByHotspot', data, callback)
  },
  registerDeviceByBluetooth: (data, callback) => {
    busEntry('registerDeviceByBluetooth', data, callback)
  },
}

window.zhiting = zhiting
window.zt = zt

export default {}
