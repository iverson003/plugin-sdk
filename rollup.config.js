import resolve from "@rollup/plugin-node-resolve"
import commonjs from "@rollup/plugin-commonjs"
import babel from "@rollup/plugin-babel"
import { uglify } from 'rollup-plugin-uglify'

export default {
  input: ['./core/plugin-sdk.js'],
  output: {
    file: './lib/plugin-sdk.runtim.min.js',
    format: 'iife',
    name: 'zhitingSdk'
  },
  plugins: [resolve(), commonjs(), babel(), uglify()],
}